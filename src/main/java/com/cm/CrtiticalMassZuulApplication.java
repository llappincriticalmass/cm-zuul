package com.cm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class CrtiticalMassZuulApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CrtiticalMassZuulApplication.class, args);
	}

}
